# How to build it
The simplest way is to build it in docker container. To do this run below steps:
* git clone
* docker run -it -v <path_to_clonned_repo_on_your_host>:/go golang:latest /bin/bash
* go get golang.org/x/net/html
* go build vipro
* ./vipro http://wiprodigital.com

## Common errors
Take a look if main.go in your docker container is in path /go/src/vipro/main.go
If it is not your probably is on /work/src/<your_dir_name>/src/vipro/main.go - which means you mounter directories wrong.

Then - on your host- go to top dir of repo and run command from this point startind from docker run 
# Trade offs
It parsing all images from wipro page. It is not very genereic to get info about images from other pages - due to puting then ion different way in my opinion - buit I'm not front end developer.

# What could be done more
* parsing images more gereic way
* Use go channels to make this really faaaaster
* Create go module so everybody can download it and use as module
* Script was created and pushed as one commit. Normally I will use git flow (create branches for each "feature", develop, etc)