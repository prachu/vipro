package main

import (
  "flag"
  "fmt"
  "net/http"
  "os"
  "strings"

  "golang.org/x/net/html"
)

//Keep all info about page
type webPageStruct struct {
  webPageName string
  linksInternal []string
  linksExternal []string
  images []string
}

//Final SiteMap
type siteMap struct {
  webPageName string
  childPages []webPageStruct
  images []string
}

//"Pretty" pritn of site map
func printStructure(mySiteMap siteMap){
  for _, v := range mySiteMap.childPages {
    fmt.Println("WebPageUrl", v. webPageName)
    for _, vv := range v.linksInternal {
      fmt.Println("  Internal link ", vv)
    }
    for _, vv := range v.linksExternal {
      fmt.Println("  External link ", vv)
    }
    for _, vv := range v.images {
      fmt.Println("  Image ", vv)
    }
  }
}

//get links from specified page
func getAllLinks(url, domain string) webPageStruct{

  links := make(map[string]int)
  images := make(map[string]int)

  var retLinksInternal []string
  var retLinksExternal []string
  var retImages []string

//Get page response
  resp, err := http.Get(url)
  if err != nil {
    fmt.Println("error")
  }
  defer resp.Body.Close()


  page := html.NewTokenizer(resp.Body)

  //for each token "classify" links
  for {
	tokenType := page.Next()
	token := page.Token()
    
    //If no more tokens exit
	if tokenType == html.ErrorToken {
      break
    }

    //"Raw" classification
    if tokenType == html.StartTagToken && token.DataAtom.String() == "a" {
	  for _, attr := range token.Attr {
        //get rid of special chars in links
	    if attr.Key == "href" {
	      if strings.Contains(attr.Val, "#") {
            attr.Val = strings.Split(attr.Val, "#")[0]
          }
          if strings.Contains(attr.Val, "?") {
            attr.Val = strings.Split(attr.Val, "?")[0]
          }
          if strings.HasSuffix(attr.Val, "/") {
            attr.Val = attr.Val[:len(attr.Val)-1]
          }
	      links[attr.Val] = 1
        //on wiprodigital.com images are put this way
	    } else if attr.Key == "style" {
	      ts := strings.Split(attr.Val, " ")
	      if ts[0] == "background:" {
            images[ts[1]] = 1
          }
	    }
	  }
    }
  }

  for val := range links{
    //check if external or interanal link
    splited := strings.Split(val, "/")
    if (len(splited) > 1) {
      if (splited[2] == domain){
        retLinksInternal = append(retLinksInternal, val)
      } else {
        retLinksExternal = append(retLinksExternal, val)
      }
    }
  }

  //put images
  for val := range images{
    retImages = append(retImages, val)
  }

  var ret webPageStruct
  ret.webPageName = url
  ret.linksInternal = retLinksInternal
  ret.linksExternal = retLinksExternal
  ret.images = retImages

  return ret
}

func main() {
  flag.Parse()

  args := flag.Args()
  if len(args) < 1 {
    fmt.Println("Please specify start page")
    os.Exit(1)
  }

  domain := strings.Split(args[0], "://")[1]

  webStruct := getAllLinks(args[0], domain)

  var mySiteMap siteMap
  mySiteMap.webPageName = webStruct.webPageName
  mySiteMap.images = webStruct.images

  //Initial load of links
  for _, v := range webStruct.linksInternal{
    new_pages := getAllLinks(v, domain)
    mySiteMap.childPages = append(mySiteMap.childPages, new_pages)
  }

  var visited_pages []string
  var non_visited_pages []string
  var non_visited_pages_map = make(map[string]int)

  //Add new sites to site map until all links will be found
  for {
    visited_pages = visited_pages[:0]
    non_visited_pages = non_visited_pages[:0]
    non_visited_pages_map = make(map[string]int)

    //collect all links which were "visited" already
    for _, v := range mySiteMap.childPages{
      visited_pages = append(visited_pages, v.webPageName)
      for _, va := range v.linksInternal {
        non_visited_pages_map[va] = 1
      }
    }

    //if checked linkes wasn't visited - visit it
    for k, _ := range non_visited_pages_map {
    foundIt := false
      for _, v :=range visited_pages {
        if v == k {
          foundIt = true
          break
        }
      }
      if foundIt {
        continue
      }
      non_visited_pages = append(non_visited_pages, k)
    }

    // go to links classified to visit and put them on site map
    for _, v := range non_visited_pages{
      mySiteMap.childPages = append(mySiteMap.childPages, getAllLinks(v, domain))
    }
	  
    //if there are no new links to visit - job is done
    if len(non_visited_pages) == 0 {
      break
    }
  }

  //print structure in "preety" way
  printStructure(mySiteMap)

}